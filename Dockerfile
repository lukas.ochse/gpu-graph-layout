#FROM nvidia/cuda:10.1-devel-ubuntu18.04
FROM nvidia/cuda:9.0-devel

# install requirements
RUN apt-get --assume-yes update
RUN apt-get --assume-yes install --fix-missing
RUN apt-get --assume-yes install git wget build-essential libpng-dev

# set working directory
WORKDIR /layout

RUN mkdir /layout/in && mkdir /layout/out

# get latest version of GPUGraphLayout
RUN cd /layout && git clone https://github.com/govertb/GPUGraphLayout.git
RUN cd GPUGraphLayout && git submodule init && git submodule update

# apply patches
COPY RPGraphLayout.cpp /layout/GPUGraphLayout/src/RPGraphLayout.cpp
COPY graph_viewer.cpp /layout/GPUGraphLayout/src/graph_viewer.cpp

# compile and set up graph viewer
RUN cd /layout/GPUGraphLayout/builds/linux && make graph_viewer -j 12 && chmod +x graph_viewer
RUN ln -s /layout/GPUGraphLayout/builds/linux/graph_viewer /usr/bin/graph_viewer

# load and prepare test data
RUN cd /layout && wget http://konect.uni-koblenz.de/downloads/tsv/ucidata-zachary.tar.bz2
RUN tar -jxf ucidata-zachary.tar.bz2 && rm ucidata-zachary.tar.bz2
RUN cd ucidata-zachary && sed -i 's/%/#/g' out.ucidata-zachary && cp out.ucidata-zachary /layout/in/graph

# run graph viewer
ENTRYPOINT ["graph_viewer"]
